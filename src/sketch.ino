#include <Servo.h>
#include "Robot.h"

Robot eHonda(
  9, 10,  // Servos
  A1, A2, // Edge detectors
  A0,     // proximity sensor
  2, 4, 3 // LEDs
);


void setup() {
  eHonda.initialize();
  Serial.begin(9600);

  Serial.println("Wait for it...");
  delay(5000);
  Serial.println("FIGHT!");
  eHonda.moveForward();
}

void loop() {
  
  if(eHonda.checkForEdges()){
    eHonda.stop();
    Serial.println("Edge found!");
    delay(2000);
    
    if(eHonda.leftEdgeDetected()){
      Serial.println("  edge on the left");
      eHonda.turnLeft();
    }
    if(eHonda.rightEdgeDetected()){
      Serial.println("  edge on the right");
      eHonda.turnRight();
    }
    
    delay(2000);
    eHonda.moveForward();
  }
  
  if(eHonda.checkForEnemy()){
    Serial.println("Enemy ahoy!");
    eHonda.moveForward();
  }else{
    Serial.print("Proximity: ");
    Serial.println(eHonda.getProximityReading());
  }
}
