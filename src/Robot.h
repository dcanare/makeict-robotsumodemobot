/**
 * @author Dominic Canare <dom@makeict.org>
 *
 **/
#include <Arduino.h>
#include <Servo.h>

class Robot {
  public:
    Robot(
      int leftServoPin, int rightServoPin,
      int leftEdgeSensePin, int rightEdgeSensePin,
      int proximitySensePin,
      int leftEdgeLED, int rightEdgeLED, int enemyDetectLED
    );

    // basic commands
    void initialize();
    boolean checkForEdges();
    boolean checkForEnemy();

    void moveForward();
    void moveBackward();
    void turnLeft();
    void turnRight();
    void stop();


    // intermediate commands
    boolean leftEdgeDetected();
    boolean rightEdgeDetected();

    // advanced commands
    void setLeftServoCenter(int leftServoCenter);
    void setRightServoCenter(int leftServoCenter);
    void setEnemyDistanceThreshold(int threshold);
    void setEdgeThreshold(int threshold);
    
    long getLeftEdgeReading();
    long getRightEdgeReading();
    long getProximityReading();

  protected:
    // INPUTS
    int leftServoPin;
    int rightServoPin;

    int leftEdgeSensePin;
    int rightEdgeSensePin;

    int proximitySensePin;
    
    // OUTPUTS
    int leftEdgeLED;
    int rightEdgeLED;
    int enemyDetectLED;

    int leftServoCenter;
    int rightServoCenter;

    Servo leftServo;
    Servo rightServo;

    void setServos(int leftDirection, int rightDirection);

    int enemyDistanceThreshold;
    int edgeThreshold;

    boolean leftEdgeFound;
    boolean rightEdgeFound;
};


// Implementation below

Robot::Robot(
  int leftServoPin, int rightServoPin,
  int leftEdgeSensePin, int rightEdgeSensePin,
  int proximitySensePin,
  int leftEdgeLED, int rightEdgeLED, int enemyDetectLED
){
  setLeftServoCenter(90);
  setRightServoCenter(90);
  setEnemyDistanceThreshold(180);
  setEdgeThreshold(180);

  Robot::leftServoPin = leftServoPin;
  Robot::rightServoPin = rightServoPin;
  
  Robot::leftEdgeSensePin = leftEdgeSensePin;
  Robot::rightEdgeSensePin = rightEdgeSensePin;

  Robot::proximitySensePin = proximitySensePin;

  Robot::leftEdgeLED = leftEdgeLED;
  Robot::rightEdgeLED = rightEdgeLED;
  Robot::enemyDetectLED = enemyDetectLED;
}

void Robot::initialize(){
  leftServo.attach(leftServoPin);
  rightServo.attach(rightServoPin);

  stop();

  pinMode(leftEdgeSensePin, INPUT);
  pinMode(rightEdgeSensePin, INPUT);
  pinMode(proximitySensePin, INPUT);

  pinMode(leftEdgeLED, OUTPUT);
  pinMode(rightEdgeLED, OUTPUT);
  pinMode(enemyDetectLED, OUTPUT);

  digitalWrite(leftEdgeLED, LOW);
  digitalWrite(rightEdgeLED, LOW);
  digitalWrite(enemyDetectLED, LOW);
}

void Robot::setServos(int left, int right){
  leftServo.write(left);
  rightServo.write(right);
}

boolean Robot::checkForEdges(){
  if(getLeftEdgeReading() < edgeThreshold){
    leftEdgeFound = true;
    digitalWrite(leftEdgeLED, HIGH);
  }else{
    leftEdgeFound = false;
    digitalWrite(leftEdgeLED, LOW);
  }
  
  if(getRightEdgeReading() < edgeThreshold){
    rightEdgeFound = true;
    digitalWrite(rightEdgeLED, HIGH);
  }else{
    rightEdgeFound = false;
    digitalWrite(rightEdgeLED, LOW);
  }

  return leftEdgeFound || rightEdgeFound;
}

boolean Robot::leftEdgeDetected(){
  return leftEdgeFound;
}

boolean Robot::rightEdgeDetected(){
  return rightEdgeFound;
}

boolean Robot::checkForEnemy(){
  if(getProximityReading() > enemyDistanceThreshold){
    digitalWrite(enemyDetectLED, HIGH);
    return true;
  }else{
    digitalWrite(enemyDetectLED, LOW);
    return false;
  }
}

long Robot::getProximityReading(){
  return analogRead(proximitySensePin);
}

long Robot::getLeftEdgeReading(){
  return analogRead(leftEdgeSensePin);
}

long Robot::getRightEdgeReading(){
  return analogRead(rightEdgeSensePin);
}

void Robot::moveForward(){
  setServos(0, 180);
}

void Robot::moveBackward(){
  setServos(180, 0);
}

void Robot::turnLeft(){
  setServos(180, 180);
}

void Robot::turnRight(){
  setServos(0, 0);
}

void Robot::stop(){
  setServos(leftServoCenter, rightServoCenter);
}

void Robot::setEnemyDistanceThreshold(int threshold){
  enemyDistanceThreshold = threshold;
}

void Robot::setEdgeThreshold(int threshold){
  edgeThreshold = threshold;
}

void Robot::setLeftServoCenter(int leftServoCenter){
  Robot::leftServoCenter = leftServoCenter;

}
void Robot::setRightServoCenter(int rightServoCenter){
  Robot::rightServoCenter = rightServoCenter;
}
