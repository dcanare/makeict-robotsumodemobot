## Super simple usage ##
* Click on the source page
* Open the 'src' folder
* Copy and paste contents from 'sketch.ino' into the Arduino IDE
* Create a new tab in the Arduino IDE (CTRL+SHIFT+n)
* Call it 'Robot.h'
* Copy and paste in contents from 'Robot.h'


### Creating a bot ###
```
#!c++
Robot eHonda(
  9, 10,  // Servos
  A1, A2, // Edge detectors
  A0,     // proximity sensor
  2, 4, 3 // LEDs
);
```

### Simple commands ###
```
#!c++
    void initialize();

    boolean checkForEdges();
    boolean checkForEnemy();

    void moveForward();
    void moveBackward();
    void turnLeft();
    void turnRight();
    void stop();
```


### Intermediate commands ###
```
#!c++
    boolean leftEdgeDetected();
    boolean rightEdgeDetected();
```

### Advanced commands ###
```
#!c++
    void setLeftServoCenter(int leftServoCenter);
    void setRightServoCenter(int leftServoCenter);

    void setEnemyDistanceThreshold(int threshold);
    void setEdgeThreshold(int threshold);
    
    long getLeftEdgeReading();
    long getRightEdgeReading();

    long getProximityReading();
```